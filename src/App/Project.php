<?php

namespace Yeltrik\Asana\App;

use Yeltrik\Asana\App\Http\Controllers\AsanaCustomFieldController;
use Asana\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Project extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customFields()
    {
        return $this->belongsToMany(CustomField::class);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function syncWithAsana() {
        if(Auth::check()) {
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $asanaProject = $asanaClient->projects->getProject($this->id);

            // Updates?
            $this->name = $asanaProject->name;
            $this->notes = $asanaProject->notes;
            $this->save();

            foreach ($asanaProject->custom_field_settings as $asanaCustomFieldSetting) {
                $customField = (new AsanaCustomFieldController())->sync($asanaCustomFieldSetting->custom_field);

                if (!$this->customFields()->find($customField->id)) {
                    $this->customFields()->attach($customField);
                }
            }
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }

}
