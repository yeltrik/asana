<?php

namespace Yeltrik\Asana\App\Http\Controllers;

use Yeltrik\Asana\App\EnumOption;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EnumOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \\Yeltrik\Asana\App\EnumOption  $enumOption
     * @return \Illuminate\Http\Response
     */
    public function show(EnumOption $enumOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \\Yeltrik\Asana\App\EnumOption  $enumOption
     * @return \Illuminate\Http\Response
     */
    public function edit(EnumOption $enumOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \\Yeltrik\Asana\App\EnumOption  $enumOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnumOption $enumOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \\Yeltrik\Asana\App\EnumOption  $enumOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnumOption $enumOption)
    {
        //
    }
}
