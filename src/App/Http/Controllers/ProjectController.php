<?php

namespace Yeltrik\Asana\App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProject;
use Yeltrik\Asana\App\Project;
use Asana\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if ( Auth::check() ) {
            $projects = Project::all();
            return view('asana::project.index', compact('projects'));
        } else {
            return redirect()->route('welcome');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Auth::check() ) {
            return view('asana::project.create');
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreProject  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProject $request)
    {
        $asanaProjectController = new ProjectController();
        $project = new Project();
        $project->id = $request->id;
        $project->name = $request->name;
        $project->save();
        $asanaProjectController->sync($project);
        return redirect()->route('projects.sync', $project);
    }

    /**
     * Display the specified resource.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        if( Auth::check()) {
            return view('asana::project.show', compact('project'));
        } else {
            return redirect()->route(   'login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param  Project  $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sync(Project $project)
    {
        if (Auth::check()) {
            $project->syncWithAsana();
            return redirect()->route('projects.show', $project);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * @param  Project  $project
     * @param  int  $limit
     * @return  string
     */
    public function taskSync(project $project)
    {
        if (Auth::check()) {
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));

            foreach ($asanaClient->tasks->getTasksForProject($project->id, [], [
                'opt_fields' => 'name, resource_type, custom_fields, completed, completed_at'
            ]) as $asanaTask) {
//                dd($asanaTask);
                $task = (new AsanaTaskController())->sync($asanaTask);

                if (!$task->projects()->find($project)) {
                    $task->projects()->attach($project);
                }
            }

            return redirect()->route('projects.show', [
                'project' => $project,
            ]);
        } else {
            return redirect()->route('login');
        }
    }

}
