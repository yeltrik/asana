<?php

namespace Yeltrik\Asana\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AsanaController extends Controller
{
    public function index()
    {
        return view('asana::index');
    }

}
