<?php

namespace Yeltrik\Asana\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Yeltrik\Asana\App\Task;
use Illuminate\Support\Facades\Auth;

class AsanaTaskController extends Controller
{

    /**
     * @param $asanaTask
     * @return Task|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function sync($asanaTask)
    {
        if ( Auth::check() ) {
            // TODO: Check to Make sure it doesn't already Exist
            $task = Task::query()->find($asanaTask->gid);
            if ($task instanceof Task !== TRUE) {
                $task = new Task();
                $task->id = $asanaTask->gid;
            }

            // Updates?
            $task->name = $asanaTask->name;
            $task->completed = $asanaTask->completed;
            $task->completed_at = $asanaTask->completed_at;
            $task->save();

            foreach ($asanaTask->custom_fields as $asanaCustomField) {
                $customField = (new AsanaCustomFieldController())->sync($asanaCustomField);

                if (!$task->customFields()->find($customField)) {
                    $task->customFields()->attach($customField);
                }

                $attributes = [];
                if (property_exists($asanaCustomField, 'enum_value') && $asanaCustomField->enum_value !== NULL) {
                    $attributes['enum_value'] = $asanaCustomField->enum_value->gid;
                }
                if (property_exists($asanaCustomField, 'number_value')) {
                    $attributes['number_value'] = $asanaCustomField->number_value;
                }
                if (property_exists($asanaCustomField, 'text_value')) {
                    $attributes['text_value'] = $asanaCustomField->text_value;
                }

                if (!empty($attributes)) {
                    $task->customFields()->updateExistingPivot($customField, $attributes);
                }
            }

            return $task;
        } else {
            return redirect()->route('login');
        }
    }

}
