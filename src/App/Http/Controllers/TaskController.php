<?php

namespace Yeltrik\Asana\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Yeltrik\Asana\App\Task;
use Asana\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( Auth::check() ) {
            return redirect()->route('projects.index');
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        if( Auth::check() ) {
            return view('asana::task.show', compact('task'));
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param  Task  $task
     */
    public function sync(Task $task)
    {
        if ( Auth::check() ) {
            $asana_client = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $asanaTask = $asana_client->tasks->getTask($task->id);
            // TODO Code to Sync a Task
            dd($asanaTask);
        } else {
            return redirect()->route('login');
        }
    }

}
