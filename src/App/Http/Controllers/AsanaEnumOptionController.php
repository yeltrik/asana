<?php

namespace Yeltrik\Asana\App\Http\Controllers;

use Yeltrik\Asana\App\EnumOption;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AsanaEnumOptionController extends Controller
{

    /**
     * @param $asanaEnumOption
     * @return EnumOption|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\RedirectResponse|null
     */
    public function sync($asanaEnumOption)
    {
        if(Auth::check()) {
            $enumOption = EnumOption::query()->find($asanaEnumOption->gid);
            if ($enumOption instanceof EnumOption === FALSE) {
                $enumOption = new EnumOption();
                $enumOption->id = $asanaEnumOption->gid;
            }

            // Updates?
            $enumOption->name = $asanaEnumOption->name;
            $enumOption->enabled = $asanaEnumOption->enabled;
            $enumOption->save();

            return $enumOption;
        } else {
            return redirect()->route('login');
        }
    }

}
