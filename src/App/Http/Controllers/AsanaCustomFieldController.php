<?php

namespace Yeltrik\Asana\App\Http\Controllers;

use Yeltrik\Asana\App\CustomField;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AsanaCustomFieldController extends Controller
{

    /**
     * @param $asanaCustomField
     * @return CustomField|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function sync($asanaCustomField)
    {
        if( Auth::check() ) {
            $customField = CustomField::query()->find($asanaCustomField->gid);
            if ($customField instanceof CustomField === FALSE) {
                $customField = new CustomField();
                $customField->id = $asanaCustomField->gid;
            }

            // Updates?
            $customField->name = $asanaCustomField->name;
            $customField->type = $asanaCustomField->type;
            $customField->save();

            if ($asanaCustomField->type === 'enum') {
                foreach ($asanaCustomField->enum_options as $asanaEnumOption) {
                    $enumOption = (new AsanaEnumOptionController())->sync($asanaEnumOption);

                    if(!$customField->enumOptions()->find($enumOption)) {
                        $customField->enumOptions()->attach($enumOption);
                    }
                }
            }

            return $customField;
        } else {
            return redirect()->route('login');
        }
    }

}
