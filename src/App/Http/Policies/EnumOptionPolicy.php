<?php

namespace Yeltrik\Asana\App\Policies;

use Yeltrik\Asana\App\EnumOption;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnumOptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnumOption  $enumOption
     * @return mixed
     */
    public function view(User $user, EnumOption $enumOption)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnumOption  $enumOption
     * @return mixed
     */
    public function update(User $user, EnumOption $enumOption)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnumOption  $enumOption
     * @return mixed
     */
    public function delete(User $user, EnumOption $enumOption)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnumOption  $enumOption
     * @return mixed
     */
    public function restore(User $user, EnumOption $enumOption)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\EnumOption  $enumOption
     * @return mixed
     */
    public function forceDelete(User $user, EnumOption $enumOption)
    {
        //
    }
}
