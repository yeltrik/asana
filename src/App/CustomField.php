<?php

namespace Yeltrik\Asana\App;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function enumOptions()
    {
        return $this->belongsToMany(EnumOption::class);
    }

}
