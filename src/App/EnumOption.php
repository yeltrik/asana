<?php

namespace Yeltrik\Asana\App;

use Illuminate\Database\Eloquent\Model;

class EnumOption extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customFields()
    {
        return $this->belongsToMany(CustomField::class);
    }

}
