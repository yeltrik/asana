<?php

namespace Yeltrik\Asana\App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customFields()
    {
        return $this->belongsToMany(CustomField::class)->withPivot('enum_value', 'number_value', 'text_value');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

}
