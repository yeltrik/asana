<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Asana\App\Http\Controllers\AsanaController;
use Yeltrik\Asana\App\Http\Controllers\ProjectController;
use Yeltrik\Asana\App\Http\Controllers\TaskController;

Route::group(['middleware' => 'web'], function () {

    Route::get('asana', [AsanaController::class, 'index'])->name('asana');

    //Route::delete('asana/task/{task}', [TaskController::class, 'destroy'])->name('tasks.destroy');
    Route::get('asana/task', [TaskController::class, 'index'])->name('tasks.index');
    //Route::get('asana/task/create', [TaskController::class, 'create'])->name('tasks.create');
    Route::get('asana/task/{task}', [TaskController::class, 'show'])->name('tasks.show');
    //Route::get('asana/task/{task}/edit', [TaskController::class, 'edit'])->name('tasks.edit');
    Route::get('asana/task/{task}/sync', [TaskController::class, 'sync'])->name('tasks.sync');
    //Route::patch('asana/task/{task}', [TaskController::class, 'update'])->name('tasks.update');
    //Route::post('asana/task', [TaskController::class, 'store'])->name('tasks.store');

    //Route::delete('asana/project/{project}', [ProjectController::class, 'destroy'])->name('projects.destroy');
    Route::get('asana/project', [ProjectController::class, 'index'])->name('projects.index');
    Route::get('asana/project/create', [ProjectController::class, 'create'])->name('projects.create');
    Route::get('asana/project/{project}', [ProjectController::class, 'show'])->name('projects.show');
    //Route::get('asana/project/{project}/edit', [ProjectController::class, 'edit'])->name('projects.edit');
    Route::get('asana/project/{project}/sync', [ProjectController::class, 'sync'])->name('projects.sync');
    Route::get('asana/project/{project}/task/sync', [ProjectController::class, 'taskSync'])->name('projects.tasks.sync');
    //Route::patch('asana/project/{project}', [ProjectController::class, 'update'])->name('projects.update');
    Route::post('asana/project', [ProjectController::class, 'store'])->name('projects.store');

});
