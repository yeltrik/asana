@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Asana</a></li>
            <li class="breadcrumb-item">
                @if($task->projects->count() == 1)
                    <a href="{{ route('projects.show', $task->projects->first()->id) }}">{{ $task->projects->first()->name }}</a>
                @else
                    @foreach($task->projects as $project)
                        <a href="{{ route('projects.show', $project->id) }}" class="mr-1">({{ $project->name }})</a>
                    @endforeach
                @endif
            </li>
            <li class="breadcrumb-item active" aria-current="page">{{ $task->name }}</li>

        </ol>
    </nav>

    <div class="d-flex justify-content-between">
        <h1>
            {{ $task->name }}
        </h1>
        <div class="d-flex">
            <a href="https://app.asana.com/0/{{ $task->projects()->first()->id }}/{{ $task->id }}/f" target="asana_{{ $task->id }}" class="btn btn-info ml-1">
                <i class="far fa-eye"></i> View in Asana
            </a>
            <a href="{{ route('tasks.sync', $task) }}">
                <button class="btn btn-primary">
                    <i class="fas fa-sync"></i> Task
                </button>
            </a>
        </div>
    </div>

</div>
@endsection
