@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Asana</a></li>
            <li class="breadcrumb-item"><a href="{{ route('projects.index') }}">Projects</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $project->name ? $project->name : '{Unnamed}' }}</li>
        </ol>
    </nav>

    <div class="accordion" id="accordion">
        <div class="card">
            <div class="card-header" id="headingMain">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseMain" aria-expanded="false" aria-controls="collapseMain">
                        <div class="d-flex justify-content-between">
                            <h1 class="text-dark">
                                {{ $project->name ? $project->name : "{Unnamed}" }}
                            </h1>
                        </div>
                    </button>
                    <div class="d-flex float-right">
                        <a href="https://app.asana.com/0/{{ $project->id }}/list" target="asana_{{ $project->id }}" class="btn btn-info ml-1">
                            <i class="far fa-eye"></i> View in Asana
                        </a>
                        <a href="{{ route('projects.sync', $project) }}" class="btn btn-primary ml-1">
                            <i class="fas fa-sync"></i> Project
                        </a>
                        @if( $project->tasks->count() == 0 )
                        <a href="{{ route('projects.tasks.sync', [
                                    'project' => $project,
                                ]) }}" class="btn btn-primary ml-1">
                            <i class="fas fa-sync"></i> Tasks
                        </a>
                        @endif
                    </div>
                </h2>

            </div>
            <div id="collapseMain" class="collapse" aria-labelledby="headingMain" data-parent="#accordion">
                <div class="card-body">
                    {{ $project->notes }}
                </div>
            </div>

            @if( $project->tasks->count() > 0 )
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTasks" aria-expanded="true" aria-controls="collapseTasks">
                            Tasks
                            <span class="badge badge-pill badge-info ml-2">
                                {{ $project->tasks()->count() }}
                            </span>
                        </button>
                        <div class="d-flex float-right">
                            <a href="{{ route('projects.tasks.sync', [
                                    'project' => $project,
                                ]) }}" class="btn btn-primary ml-1">
                                <i class="fas fa-sync"></i> Tasks
                            </a>
                        </div>
                    </h2>
                </div>
                <div id="collapseTasks" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
{{--                        @include('project.tasks')--}}
{{--                        @yield('table')--}}

                        @if( $project->tasks->count() > 0 )
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Completed</th>
                                    @foreach($project->customFields as $customField)
                                        <th scope="col">
                                            {{ $customField->name }}
                                        </th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($project->tasks as $task)
                                    <tr>
                                        <th scope="row">
                                            <a href="{{ route('tasks.show', $task) }}">
                                                {{ $task->name }}
                                            </a>
                                        </th>
                                        <td>
                                            @if($task->completed)
                                                <button class="btn btn-success">
                                                    <i class="fas fa-check"></i>
                                                    {{ $task->completed_at }}
                                                </button>
                                            @else
                                                &nbsp;
                                            @endif
                                        </td>
                                        @foreach($project->customFields as $customField)
                                            @if(!$task->customFields->find($customField))
                                                <td scope="col" class="disabled">
                                                    needs sync...
                                                </td>
                                            @else
                                                <td scope="col">
                                                    @if($task->customFields->find($customField)->pivot->enum_value !== NULL)
                                                        @if(\Yeltrik\Asana\App\EnumOption::query()->find($task->customFields->find($customField)->pivot->enum_value) !== NULL)
                                                            {{ \Yeltrik\Asana\App\EnumOption::query()->find($task->customFields->find($customField)->pivot->enum_value)->name }}
                                                        @else
                                                            (ERROR: Sync)
                                                        @endif
                                                    @endif
                                                    {{ $task->customFields->find($customField)->pivot->number_value }}
                                                    {{ $task->customFields->find($customField)->pivot->text_value }}
                                                </td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

</div>
@endsection
