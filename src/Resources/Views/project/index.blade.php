@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Asana</a></li>
            <li class="breadcrumb-item active" aria-current="page">Projects</li>
        </ol>
    </nav>

    <a href="{{ route('projects.create') }}" class="btn btn-primary float-right">
        <i class="far fa-plus-square"></i> Project
    </a>

    @foreach($projects as $project)
        <div>
            <a href="{{ route('projects.show', $project) }}">
                {{ $project->name ? $project->name : '{Unnamed}' }}
            </a>
            <span class="badge badge-pill badge-info">
                {{ $project->tasks()->count() }}
            </span>
        </div>
    @endforeach
</div>
@endsection
