@section('table')

@if( $project->tasks->count() > 0 )
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Name</th>
        @foreach($project->customFields as $customField)
            <th scope="col">
                {{ $customField->name }}
            </th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($project->tasks as $task)
        <tr>
            <th scope="row">
                <a href="{{ route('tasks.show', $task) }}">
                    {{ $task->name }}
                </a>
            </th>
            @foreach($project->customFields as $customField)
                @if(!$task->customFields->find($customField))
                    <th scope="col" class="disabled">
                        needs sync...
                    </th>
                @else
                    <th scope="col">
                        @if($task->customFields->find($customField)->pivot->enum_value !== NULL)
                            @if(\Yeltrik\Asana\App\EnumOption::query()->find($task->customFields->find($customField)->pivot->enum_value) !== NULL)
                                {{ \\Yeltrik\Asana\App\EnumOption::query()->find($task->customFields->find($customField)->pivot->enum_value)->name }}
                            @else
                                (ERROR: Sync)
                            @endif
                        @endif
                        {{ $task->customFields->find($customField)->pivot->number_value }}
                        {{ $task->customFields->find($customField)->pivot->text_value }}
                    </th>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
    @endif
@endsection
